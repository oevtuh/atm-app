var gulp = require('gulp');
var rjs = require('gulp-requirejs-optimize');
var rename = require('gulp-rename');
var postCss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var concat = require('gulp-concat');

gulp.task('scripts', function () {
    return gulp.src('src/app/js/main.js')
        .pipe(rjs({
            mainConfigFile: 'src/app/js/main.js',
            out: 'app.js',
            optimize: 'none'
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('html', function () {
    gulp.src('src/app/index.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('r', function () {
    gulp.src('src/app/bower_components/requirejs/require.js')
        .pipe(rename('r.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('css', function () {
    gulp.src('src/app/assets/css/*.css')
        .pipe(postCss([
            autoprefixer({browsers: ['last 1 version']}),
            cssnano()
        ]))
        .pipe(gulp.src([
            'src/app/bower_components/bootstrap/dist/css/bootstrap.min.css',
            'src/app/bower_components/bootstrap/dist/css/bootstrap-theme.min.css'
        ]))
        .pipe(concat('styles.css'))        //.pipe(concat('styles.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['scripts', 'html', 'css', 'r']);
