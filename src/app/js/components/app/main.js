define(['angular', 'components/app/directives/app'], function (angular) {
    return angular.module('app', ['app.directives']);
});
