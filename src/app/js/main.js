require.config({
    paths: {
        // Libs
        angular: '../bower_components/angular/angular.min',
        jquery: '../bower_components/jquery/dist/jquery.min',
        bootstrap: '../bower_components/bootstrap/dist/js/bootstrap.min',

        // App
        app: 'components/app/main'
    },
    shim: {
        angular: {
            exports: 'angular',
            deps: [
                'jquery'
            ]
        },
        bootstrap: {
            deps: [
                'jquery'
            ]
        }
    }
});

require(['angular', 'bootstrap', 'app'], function (angular) {
    angular.bootstrap(window.document.body, ['app']);
});
